
function amount(x1, x2) {
    if (x1>x2) {
        var temp=x1;
          x1=x2;
          x2=temp;
    }
    var amount=0;
        for (var i=x1; i<=x2; i++) {
            amount+=i;
        } 
    return amount;  
}

function multiply(x1, x2) {
    var multiply=1;
    if (x1>x2) {
      var temp=x1;
        x1=x2;
        x2=temp;
    }
    for (var i=x1; i<=x2; i++) {
        multiply*=i;
    }   
    return multiply;
}

function simpleNumber(x1, x2) {
  var arraySimpleNumber=[];   
  for (var i=x1; i<=x2; i++) {
    var isSimple=true;
    if (i=='1') {isSimple=false;}
    for (var j = 2; j < i; j++) {
      if (i % j == 0) {isSimple=false; break;}
    }
    if (isSimple) 
        arraySimpleNumber.push(i);
  }

  return arraySimpleNumber;
}



function buttonClick() {
    var x1=document.getElementById('x1').value;
    var x2=document.getElementById('x2').value;

    if ((x1=='')||(x2=='')) {
        alert('Поля x1 и x2 должны быть заполнены!');
    } else {
        x1=parseInt(x1);
        x2=parseInt(x2);
        if (Number.isNaN(x1) || Number.isNaN(x2)) {
            alert('В поля х1 и х2 должны быть введены числовые значения!');
        } else {
            var resultDiv=document.getElementById('result');
            var checkedList=document.getElementsByName('Choose');
            console.log(checkedList);
            for (var checkedElement of checkedList) {
                console.log(checkedElement);
                if (checkedElement.checked) {
                    switch (checkedElement.value) {
                        case 'Sum' : {
                            resultDiv.innerHTML=('Сумма чисел от '+x1+' до '+x2+' равна '+amount(x1, x2));
                            break;
                        }
                        case 'multiply': {
                            resultDiv.innerHTML=('Произведение чисел от '+x1+' до '+x2+' равна '+multiply(x1, x2));
                            break;
                        }
                        case 'simpleNumber': {
                            resultDiv.innerHTML=('Простые числа от '+x1+' до '+x2+': '+simpleNumber(x1, x2));
                            break;
                        }
                    }  
                } 
            }  
        }
    }
}

function buttonClear () {
    document.getElementById('x1').value='';
    document.getElementById('x2').value='';
}





